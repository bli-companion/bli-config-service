package com.project.bli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class BliConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BliConfigServiceApplication.class, args);
	}
}
